/*CREATE TABLE user (
	user_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_firstname varchar(255),
	user_lastname varchar(255),
	user_phone int,
	user_town varchar(255),
	user_score int DEFAULT 0,
	user_email varchar(255),
	user_username varchar(255),
	user_password varchar(255),
	user_type int(1),
	user_join_date TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE foundation (
	foundation_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
	foundation_name varchar(255),
	foundation_addressline1 varchar(255),
	foundation_addressline2 varchar(255),
	foundation_zipcode varchar(5),
	foundation_desc varchar(255),
	foundation_town varchar(255),
	foundation_phone varchar(11),
	foundation_email varchar(255)
);

CREATE TABLE user_record (
	user_record_id int ,
	user_id int,
	user_record_score int,
	user_record_history varchar(255),
	user_record_comments varchar(255),
	FOREIGN KEY (user_id)
		REFERENCES user(user_id)
		ON DELETE CASCADE
);*/


/*CREATE AND INSERT TEST DATA IN DB*/

/*INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Residencial Luis Llorens Torres','','00914','Atrévete, Inc.','','San Juan','7872685445','saridelgado@hotmail.com');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('P.O. Box 9399665','', '00928','Boys and Girls Club of PR','','San Juan','7877264701','jcampos@bgcpr.com');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Carr. #3, KM 29.5','','00745','Carola Home Care','','Rio Grande','7878878740','');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('37 Calle Lucero','Urb. El Verde','00725','Casa San Gerardo','','Caguas','7877462827','');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Calle Pumarada 1708','','00912','Centro Beth Yash\'ah','','Santurce','7877274100','');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Hospital San Lucas II','Carr. 14, Bo. Machuelos','00732','Albergue La Providencia, Inc','','Ponce','787412119','');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Ave. Tito Castro 609','Suite 102 PMB 442','00717','Casa San Gerardo','','Caguas','7873971217','');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('37 Calle Lucero','Urb. El Verde','00725','Casa San Gerardo','','Caguas','7877462827','');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Calle Nelson Colón','Final zona industrial Guanajibo','00725','Asociación Mayagüezana de Personas con Impedimentos, Inc.','','Mayagüez','7878327460','pcemecav@prtc.net');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('','','00681','Hogar Albergue para Niños Jesús de Nazaret, Inc.','','Mayagüez','7878316161','childnom@coqui.net');

INSERT INTO foundation (foundation_addressline1, foundation_addressline2, foundation_zipcode, foundation_name, foundation_desc, foundation_town, foundation_phone, foundation_email)
VALUES ('Calle Dr.Pedro Albizu Campos #50','','00669','Hogar de Envejecientes Irma Fe Pol Méndez','','Lares','7878976090','');

SELECT *
FROM foundation;*/