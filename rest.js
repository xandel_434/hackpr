function REST_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes= function(router,connection,md5) {
    router.get("/",function(req,res){
        res.json({"Error" : "This is not the API you\'re looking for..."});
    })

    router.post("/reg", function(req,res){
    	var passhash = "";
    	var query = "INSERT INTO user (user_firstname, user_lastname, user_phone, user_town, user_email, user_username, user_password, user_type) ";
    		query += "VALUES ('"+req.body.firstname+"', '"+req.body.lastname+"', '"+req.body.phone+"', '"+req.body.town+"', '"+req.body.email+"', '"+req.body.username+"', '"+md5(req.body.password)+"', '"+req.body.type+"')";
    		console.log(query);
    		connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "MySQL couldn't complete due to errors."});
            } else {
                res.json({"Error" : false, "Message" : "User added succesfully."});
            }
        });
    });

    router.get("/foundations", function(req,res){
		var query = "SELECT * FROM foundation";
		connection.query(query,function(err,rows){
            if(err) {
	            console.log("MySQL couldn't complete due to errors.");
                //res.json({"Error" : true, "Message" : "MySQL couldn't complete due to errors."});
            } else {
	            console.log("Data fetched successfully.");
                //res.json({"Error" : false, "Message" : "Data fetched successfully."});
                var objs = [];
				for (var i = 0;i < rows.length; i++) {
					objs.push({
						id: rows[i].foundation_id,
						name: rows[i].foundation_name,
						address1: rows[i].foundation_addressline1,
						address2: rows[i].foundation_addressline2,
						zipcode: rows[i].foundation_zipcode,
						description: rows[i].foundation_desc,
						town: rows[i].foundation_town,
						phone: rows[i].foundation_phone,
						email: rows[i].foundation_email,
						img: rows[i].foundation_img,
					});
				}

					res.end(JSON.stringify(objs));
            }
        });
	});

	router.post("/login", function(req,res){
		var query = "SELECT user_password FROM user WHERE user_username = \'"+req.body.username+"\'";
		connection.query(query, function(err,rows){
			if (err) {
				console.log("MySQL couldn't complete due to errors.");
				console.log(query);
			}
			else {
				if(rows[0].user_password === md5(req.body.password)){
					res.json({"code" : "1"});
				}
				else{
					res.json({"code" : "0"});
				}
			}	
		});
	});
}

module.exports = REST_ROUTER;